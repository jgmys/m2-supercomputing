PROGRAM hello_world
  IMPLICIT NONE

  !$omp parallel
  PRINT*,'hello'
  !$omp end parallel

END PROGRAM hello_world
