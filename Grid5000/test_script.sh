#!/bin/bash

machines=`uniq $OAR_NODEFILE`
for m in $machines
do
    echo Execution on the machine $m .....:
    oarsh $m echo `date` > $m &
    i=0
    while [ $i -ne 50000 ]
    do
        let "i=i+1"
        echo $i > counting_$m
    done
done
