#include <stdio.h>
#include <math.h>
#include <omp.h>

#include "mmul_host.h"

// naive matrix multiply ikj order
void
matmulHost(const float * A, const float * B, float * C, int size)
{
    printf("OpenMP naive CPU mmul (ikj) with %d threads...\n", omp_get_num_threads());

    float sum = 0.0;

    #pragma omp parallel for
    for (int i = 0; i < size; ++i) {
        for (int k = 0; k < size; ++k) {
            float aik = A[i * size + k];
            for (int j = 0; j < size; ++j) {
                C[i * size + j] += aik * B[k * size + j];
            }
        }
    }
}

bool
compareResults(const float * arrA, const float * arrB, const int size, const float eps)
{
    for (int i = 0; i < size * size; ++i) {
        if (fabs(arrA[i] - arrB[i]) > eps) return false;
    }

    return true;
}
