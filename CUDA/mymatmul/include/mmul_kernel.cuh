#ifndef MMUL_KERNEL_H
#define MMUL_KERNEL_H

#include <stdio.h>

#define TILE_WIDTH 32

// ERROR CHECKING MACRO
#define gpuErrchk(ans)                                                         \
    { gpuAssert((ans), __FILE__, __LINE__); }
inline void
gpuAssert(cudaError_t code, const char * file, int line,
  bool abort = true)
{
    if (code != cudaSuccess) {
        fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file,
          line);
        if (abort)
            exit(code);
    }
}

// kernels
__global__ void
matmul2D(float * A, float * B, float * C, int size);
__global__ void
matmul2D_smem(float * A, float * B, float * C, int size);
__global__ void
matmul2D_smem_24(float * A, float * B, float * C, int size);

#endif // ifndef MMUL_KERNEL_H
