#include <stdio.h> //printf,...
#include <string.h> //memset
#include <stdlib.h> //calloc
#include <math.h> //atan //link with -lm
#include <time.h> //clock_gettime,...
#include <omp.h>

#include "../gpuerrchk.h"

/*CUDA : JACOBI*/
/*T = double or float ... (depending on the GPU this can make a big performance difference !!!) */
/* =========================================================*/
template<typename T>
__global__ void jacobi_step(const T hxhy, const int nx, const int ny, const T* f, T *in, T *out)
{
    int tx = blockIdx.x*blockDim.x+threadIdx.x;
    int ty = blockIdx.y*blockDim.y+threadIdx.y;

    if(tx==0 || ty==0)return;
    if(tx==nx-1 || ty==ny-1)return;

    out[ty*nx+tx]=0.25*(\
        in[ty*nx+tx+1]+\
        in[ty*nx+tx-1]+\
        in[(ty-1)*nx+tx]+\
        in[(ty+1)*nx+tx]- \
        hxhy*f[ty*nx+tx]);
}

template<typename T>
void cudaJacobi(const int niter, const T hx, const T hy, const int nx, const int ny, const T *f, T* u){
    //right-hand side
    T *f_d;
    gpuErrchk( cudaMalloc(&f_d,nx*ny*sizeof(T)) );
    gpuErrchk( cudaMemcpy(f_d,f,nx*ny*sizeof(T),cudaMemcpyHostToDevice) );

    //in/out buffers
    T *in_d;
    T *out_d;
    gpuErrchk( cudaMalloc(&in_d,nx*ny*sizeof(T)) );
    gpuErrchk( cudaMalloc(&out_d,nx*ny*sizeof(T)) );
    gpuErrchk( cudaMemset(in_d,0,nx*ny*sizeof(T)) );//set to 0

    dim3 nblocks((nx+15)/16,(ny+15)/16); /*2D blocks...*/
    dim3 blocksize(16,16); /*...of size (16,16)*/

    T hxhy = hx*hy;

    for(int i=0;i<niter;i++){
        jacobi_step<<<nblocks,blocksize>>>(hxhy,nx,ny,f_d,in_d,out_d);
        //swap in/out pointers (avoid copy out->in)
        T* tmp=in_d;
        in_d=out_d;
        out_d=tmp;
    }

    //check for error in kernel
    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );//for error check
    //get result
    gpuErrchk( cudaMemcpy(u, in_d, nx*ny*sizeof(T),cudaMemcpyDeviceToHost) );
}


/*CUDA : RED-BLACK GAUSS-SEIDEL*/
/* =========================================================*/
template<typename T>
__global__ void rb_jacobi_evenstep(const T hxhy, const int nx, const int ny, const T* f, T *inout)
{
    int tx = blockIdx.x*blockDim.x+threadIdx.x;
    int ty = blockIdx.y*blockDim.y+threadIdx.y;

    int indY = ty;
    /*in odd rows,take odd columns - in even rows, take even columns */
    int indX = 2*tx + (ty&1U);

   if(indX==0 || indY==0 || indX == nx-1 || indY == ny-1)return;

   inout[indY*nx+indX]=0.25*( \
        inout[indY*nx+indX+1]+\
        inout[indY*nx+indX-1]+\
        inout[(indY-1)*nx+indX]+\
        inout[(indY+1)*nx+indX]-\
        hxhy*f[indY*nx+indX]);
}

template<typename T>
__global__ void rb_jacobi_oddstep(const T hxhy, const int nx, const int ny, const T* f, T *inout)
{
   int tx = blockIdx.x*blockDim.x+threadIdx.x;
   int ty = blockIdx.y*blockDim.y+threadIdx.y;

    int indY = ty;
    /*in odd rows,take even columns - in even rows, take odd columns */
    int indX = 2*tx + (ty&1U) + 1;

    /*nothing to do on border*/
   if(indX==0 || indY==0 || indX == nx-1 || indY == ny-1)return;

   inout[indY*nx+indX]=0.25*( \
        inout[indY*nx+indX+1]+\
        inout[indY*nx+indX-1]+\
        inout[(indY-1)*nx+indX]+\
        inout[(indY+1)*nx+indX]-\
        hxhy*f[indY*nx+indX]);
}

template<typename T>
void cudaRBJacobi(const int niter, const T hx, const T hy, const int nx, const int ny, const T* f, T* u){
    T *f_d;
    gpuErrchk( cudaMalloc(&f_d,nx*ny*sizeof(T)) );
    gpuErrchk( cudaMemcpy(f_d,f,nx*ny*sizeof(T),cudaMemcpyHostToDevice) );

    T *inout_d;
    gpuErrchk( cudaMalloc(&inout_d,nx*ny*sizeof(T)) );
    gpuErrchk( cudaMemset(inout_d,0,nx*ny*sizeof(T)) );//set to 0

    dim3 nblocks((nx+15)/(2*16),(ny+15)/16);
    dim3 blocksize(16,16);

    T hxhy = hx*hy;

    for(int i=0;i<niter;i++){
        /*need two separate kernels for global synchronization between steps!!!*/
        rb_jacobi_evenstep<<<nblocks,blocksize>>>(hxhy,nx,ny,f_d,inout_d);
        rb_jacobi_oddstep<<<nblocks,blocksize>>>(hxhy,nx,ny,f_d,inout_d);
    }
    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );//just for error checking

    gpuErrchk( cudaMemcpy(u, inout_d, nx*ny*sizeof(T),cudaMemcpyDeviceToHost) );
}



template<typename T>
void gauss_seidel(const int niter, const T hx, const T hy, const int nx, const int ny, const T *f, T* u){
    T *in=(T*)calloc(nx*ny,sizeof(T));
    T *out=(T*)calloc(nx*ny,sizeof(T));

    //avoid some useless computation...
    const T hxhy= hx*hy;

    //iterations
    for(int k=0;k<niter;k++){
        //jacobi sweep
        for(int i=1;i<nx-1;i++){
            for(int j=1;j<ny-1;j++){
                out[i*nx+j]=0.25*(\
                    in[i*nx+j+1]+\
                    out[i*nx+j-1]+\   // this...
                    out[(i-1)*nx+j]+\ // ...makes loop parallelization impossible
                    in[(i+1)*nx+j]-\
                    hxhy*f[i*nx+j]);
            }
        }

        //swap buffer
        T* tmp=in;
        in=out;
        out=tmp;
    }

    memcpy(u, in, nx*ny*sizeof(T));
    free(in);
    free(out);
}

template<typename T>
void redblack_gauss_seidel(const int niter, const T hx, const T hy, const int nx, const int ny, const T* f, T* u){
    //declare & fill buffer with 0s
    T* inout;
    inout=(T*)calloc(nx*ny,sizeof(T));

    // memcpy(buf, u, nx*ny*sizeof(T));

    //avoid some useless computation...
    const T hxhy=hx*hy;

    //iterations
    #pragma omp parallel
    {
    for(int k=0;k<niter;k++){
        //jacobi sweep
        //EVEN
        #pragma omp for //schedule(static)
        for(int i=1;i<nx-1;i++){
            int odd=i&1U;//%2;
            for(int j=2-odd;j<nx-1;j+=2)
                inout[i*nx+j]=0.25*( \
                    inout[i*nx+j+1]+ \
                    inout[i*nx+j-1]+ \
                    inout[(i-1)*nx+j]+ \
                    inout[(i+1)*nx+j]- \
                    hxhy*f[i*nx+j]);
        }

        //jacobi sweep
        //ODD
        #pragma omp for //for schedule(runtime)
        for(int i=1;i<nx-1;i++){
            int odd=i&1;//%2;
            for(int j=1+odd;j<ny-1;j+=2)
                inout[i*nx+j]=0.25*( \
                inout[i*nx+j+1]+ \
                inout[i*nx+j-1]+ \
                inout[(i-1)*nx+j]+ \
                inout[(i+1)*nx+j]- \
                hxhy*f[i*nx+j]);
        }
    }
    }

    memcpy(u, inout, nx*ny*sizeof(T));
    free(inout);
}

template<typename T>
void jacobi(const int niter, const T hx, const T hy, const int nx, const int ny, const T* f, T* u){
    T *in = (T*)calloc(nx*ny,sizeof(T));
    T *out = (T*)calloc(nx*ny,sizeof(T));

    //avoid some useless computation...
    const T hxhy=hx*hy;

    //iterations
    // #pragma omp parallel
    for(int k=0;k<niter;k++){
        //jacobi sweep
        // #pragma omp for schedule(static)
        for(int i=1;i<nx-1;i++){
            for(int j=1;j<ny-1;j++){
                out[i*nx+j]=0.25*((in[i*nx+j+1]+in[i*nx+j-1]+in[(i-1)*nx+j]+in[(i+1)*nx+j])-hxhy*f[i*nx+j]);
            }
        }

        //swap in/out pointers
        T* tmp=in;
        in=out;
        out=tmp;
    }

    memcpy(u, in, nx*ny*sizeof(T));

    free(in);
    free(out);
}

template <typename T>
T norm2(T* vec, int len){
    T n2=0.0;
    for(size_t i=0;i<len;i++){
        n2 += vec[i]*vec[i];
    }
    return sqrt(n2);
}

template <typename T>
T getError(T* u, T* uex, int nx, int ny)
{
    //compute error
    T tmp=0;
    for(int i=0;i<nx;i++){
        for (int j=0;j<ny;j++){
            tmp+=pow(u[i*nx+j]-uex[i*nx+j],2.0);
        }
    }
    return sqrt(tmp)/norm2(uex, nx*ny);
}

template <typename T>
void writeResultToFile(int nx, int ny, T* x, T* y, T* u, T* uex)
{
    /* write solution, exact solution and local error in result file
    to visualize with gnuplot ... start gnuplot and type ...
    set pm3d; set hidden3d;
    splot 'results_jacobi.dat' using 1:2:3 (computed solution)
    splot 'results_jacobi.dat' using 1:2:4 (exact solution)
    splot 'results_jacobi.dat' using 1:2:5 (local error)*/
    FILE *fd;
    fd = fopen("./results_jacobi.dat", "w");
    for(int i=0;i<nx;i++){
        for (int j=0;j<ny;j++){
            fprintf(fd, "%f\t%f\t%f\t%f\t%f\n",x[i],y[j],u[i*nx+j],uex[i*nx+j],u[i*nx+j]-uex[i*nx+j]);
        }
        fprintf(fd, "\n");
    }
    fclose(fd);
}


int main(int argc,char** argv)
{
    const int niter=20000;

    const int n=512;//nb points
    const int nx=n;
    const int ny=n;

    typedef float T; //T is now an alias for double... (can switch to float)

    //alloc and zero-init
    T* u =(T*)malloc(nx*ny*sizeof(T));
    //rhs and exact solution
    T* f=(T*)malloc(nx*ny*sizeof(T));
    T* uex=(T*)malloc(nx*ny*sizeof(T));
    //grid coordinates
    T* x=(T*)malloc(nx*sizeof(T));
    T* y=(T*)malloc(ny*sizeof(T));

    //========================================
    T hx,hy;
    //compute mesh
    hx=1.0/(nx-1);
    for(int i=0;i<nx;i++)x[i]=i*hx;
    hy=1.0/(ny-1);
    for(int i=0;i<ny;i++)y[i]=i*hy;

    printf("Mesh size:\t %dx%d / %f %f\n",nx,ny,hx,hy);

    //compute rhs and exact sol
    const T pi=4.0*atan(1.0);
    for(int i=0;i<nx;i++){
        for (int j=0;j<ny;j++){
            uex[i*nx+j]=sin(pi*x[i])*sin(pi*y[j]);
            f[i*nx+j]=-2.0*pi*pi*uex[i*nx+j];
        }
    }

    //solve linear problem
    struct timespec start,stop;

    printf(" ======= CPU Jacobi\n");
    clock_gettime(CLOCK_MONOTONIC,&start);
    jacobi(niter, hx, hy, nx, ny, f, u);
    clock_gettime(CLOCK_MONOTONIC,&stop);
    printf("Error:\t %f\n",getError(u,uex,nx,ny));
    printf("Elapsed:\t %2.10f\n",(stop.tv_sec-start.tv_sec)+(stop.tv_nsec-start.tv_nsec)/1e9);
    // writeResultToFile(nx,ny,x,y,u,uex);

    printf(" ======= CUDA Jacobi\n");
    clock_gettime(CLOCK_MONOTONIC,&start);
    cudaJacobi(niter,hx,hy,nx,ny,f,u);
    clock_gettime(CLOCK_MONOTONIC,&stop);
    printf("Error:\t %f\n",getError(u,uex,nx,ny));
    printf("Elapsed:\t %2.10f\n",(stop.tv_sec-start.tv_sec)+(stop.tv_nsec-start.tv_nsec)/1e9);
    // writeResultToFile(nx,ny,x,y,u,uex);

    printf(" ======= CPU Gauss-Seidel\n");
    clock_gettime(CLOCK_MONOTONIC,&start);
    gauss_seidel(niter, hx, hy, nx, ny, f, u);
    clock_gettime(CLOCK_MONOTONIC,&stop);
    printf("Error:\t %f\n",getError(u,uex,nx,ny));
    printf("Elapsed:\t %2.10f\n",(stop.tv_sec-start.tv_sec)+(stop.tv_nsec-start.tv_nsec)/1e9);
    // writeResultToFile(nx,ny,x,y,u,uex);

    printf(" ======= CPU Red-Black Gauss-Seidel\n");
    clock_gettime(CLOCK_MONOTONIC,&start);
    redblack_gauss_seidel(niter, hx, hy, nx, ny, f, u);
    clock_gettime(CLOCK_MONOTONIC,&stop);
    printf("Error:\t %f\n",getError(u,uex,nx,ny));
    printf("Elapsed:\t %2.10f\n",(stop.tv_sec-start.tv_sec)+(stop.tv_nsec-start.tv_nsec)/1e9);
    // writeResultToFile(nx,ny,x,y,u,uex);

    printf(" ======= CUDA Red-Black GS\n");
    clock_gettime(CLOCK_MONOTONIC,&start);
    cudaRBJacobi(niter,hx,hy,nx,ny,f,u);
    clock_gettime(CLOCK_MONOTONIC,&stop);
    printf("Error:\t %f\n",getError(u,uex,nx,ny));
    printf("Elapsed:\t %2.10f\n",(stop.tv_sec-start.tv_sec)+(stop.tv_nsec-start.tv_nsec)/1e9);
    // writeResultToFile(nx,ny,x,y,u,uex);

    //free memory
    free(u);
    free(f);
    free(uex);
    free(x);
    free(y);
}
