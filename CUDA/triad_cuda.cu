#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "gpuerrchk.h"

#define BLOCKSIZE 128

void triad(const float* a,const float* b,const float* c,float* d, const size_t n,const size_t m)
{
    for(int i=0;i<n;i++){
        for(int j=0;j<m;j++){
            d[j]=a[j]*b[j]+c[j];
        }
    }
}

__global__ void
triad_kernel(const float * a, const float * b, const float * c, float * d, size_t m)
{
    int tid = blockIdx.x * blockDim.x + threadIdx.x;

    if(tid < m){
        d[tid]=a[tid]+b[tid]*c[tid];
    }
}

void triad_cuda(const float* a,const float* b,const float* c,float* d, const size_t n,const size_t m)
{
    dim3 nbblocks((m+BLOCKSIZE-1)/BLOCKSIZE);

    for(size_t i=0;i<n;i++){
        triad_kernel<<<nbblocks,BLOCKSIZE>>>(a,b,c,d,m);
        cudaDeviceSynchronize();
    }
}


int main(int argc,char** argv)
{
    size_t m;//aka long unsigned int

    if(argc==2){
        m = atoi(argv[1]);
    }else{
        printf("need an argument (vector length)!\n");
    }

    unsigned long long int n=(1ULL<<32);
    n /= m;

    float *a,*b,*c,*d;
    a=(float*)malloc(m*sizeof(float));
    b=(float*)malloc(m*sizeof(float));
    c=(float*)malloc(m*sizeof(float));
    d=(float*)malloc(m*sizeof(float));

    for(int i=0;i<m;i++){
        a[i]=i+1;
        b[i]=i+2;
        c[i]=i+3;
    }

    //allocate on GPU
    float *a_d,*b_d,*c_d,*d_d;
    cudaMalloc(&a_d,m*sizeof(float));
    cudaMalloc(&b_d,m*sizeof(float));
    cudaMalloc(&c_d,m*sizeof(float));
    cudaMalloc(&d_d,m*sizeof(float));

    //copy to device
    cudaMemcpy(a_d,a,m*sizeof(float),cudaMemcpyHostToDevice);
    cudaMemcpy(b_d,b,m*sizeof(float),cudaMemcpyHostToDevice);
    cudaMemcpy(c_d,c,m*sizeof(float),cudaMemcpyHostToDevice);

    struct timespec tstart,tstop;

    clock_gettime(CLOCK_MONOTONIC,&tstart);
    triad_cuda(a_d,b_d,c_d,d_d,n,m);
    clock_gettime(CLOCK_MONOTONIC,&tstop);

    double elapsed=(tstop.tv_sec-tstart.tv_sec)+(tstop.tv_nsec-tstart.tv_nsec)/1e9;
    printf("m=%lu %llu \t elapsed time: %f \t Mflops: %f \t BW[GB/s]: %f \n", m, n, elapsed,
      (2.0 * n * m / elapsed) / (1024.0 * 1024.0), \
      (4*sizeof(float)*m*n/elapsed)/(1024.0*1024.0*1024.0));

    cudaFree(a_d);
    cudaFree(b_d);
    cudaFree(c_d);
    cudaFree(d_d);

    free(a);
    free(b);
    free(c);
    free(d);
}
