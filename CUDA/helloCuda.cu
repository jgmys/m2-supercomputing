#include <stdio.h>

__global__ void helloCuda()
{
	printf("hello from thread %d\n",threadIdx.x);
	if(threadIdx.x == 0)
		printf("==========\n");
}

int main(){
	helloCuda<<<1,1>>>();
	cudaDeviceSynchronize();
	
	helloCuda<<<1,64>>>();
	cudaDeviceSynchronize();
}
